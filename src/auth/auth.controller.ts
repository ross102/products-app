import {
  Body,
  Controller,
  Get,
  Post,
  UseGuards,
  Request,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { AuthGuard } from 'src/auth/auth.guard';
import { AuthService } from './auth.service';
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}
  //signup
  @Post('/signup')
  async addUser(
    @Body('password') userPassword: string,
    @Body('username') userName: string,
  ) {
    const saltOrRounds = 10;
    const hashedPassword = await bcrypt.hash(userPassword, saltOrRounds);
    const result = await this.authService.insertUser(userName, hashedPassword);
    return {
      msg: 'User successfully registered',
      userId: result.id,
      userName: result.username,
    };
  }
  //Post / Login

  @Post('/login')
  async login(
    @Body('password') userPassword: string,
    @Body('username') userName: string,
  ): Promise<any> {
    const result = await this.authService.validateUser(userName, userPassword);
    return result;
  }
  //Get / protected
  @UseGuards(AuthGuard)
  @Post('/create')
  async createOrder(@Body() bodyres) {
    const order = await this.authService.createOrder(
      bodyres.name,
      bodyres.price,
      bodyres.quantity,
    );
    return order;
  }
  @UseGuards(AuthGuard)
  @Get('/orders')
  async getOrders() {
    const orders = await this.authService.getOrders();
    return orders;
  }
}
