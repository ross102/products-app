import { Injectable, NotAcceptableException } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { ProductService } from 'src/products/products.service';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private productService: ProductService,
    private jwtService: JwtService,
  ) {}
  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.usersService.getUser(username);
    const passwordValid = await bcrypt.compare(password, user.password);
    if (!user) {
      throw new NotAcceptableException('could not find the user');
    }
    if (user && passwordValid) {
      const payload = {
        sub: user.id,
        userName: user.username,
      };
      return {
        access_token: await this.jwtService.signAsync(payload),
        ...payload,
      };
    }
    return null;
  }
  async insertUser(userName: string, password: string) {
    const newuser = await this.usersService.insertUser(userName, password);
    return newuser;
  }

  async createOrder(name: string, price: string, quantity: string) {
    const newItem = await this.productService.createOrder(
      name,
      price,
      quantity,
    );
    return newItem;
  }

  async getOrders() {
    const newItems = await this.productService.getItems();
    return newItems;
  }
}
