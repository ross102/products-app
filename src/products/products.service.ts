import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Product } from './products.model';
@Injectable()
export class ProductService {
  constructor(@InjectModel('product') private productModel: Model<Product>) {}
  async createOrder(name: string, price: string, quantity: string) {
    const productname = name.toLowerCase();
    const newOrder = new this.productModel({
      name: productname,
      price,
      quantity,
    });
    await newOrder.save();
    return newOrder;
  }
  async getItems() {
    const products = await this.productModel.find({});
    return products;
  }
}
