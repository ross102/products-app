import * as mongoose from 'mongoose';
export const ProductSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      unique: true,
    },
    price: {
      type: String,
      required: true,
    },
    quantity: {
      type: String,
      required: true,
    },
  },
  { timestamps: true },
);

export interface Product extends mongoose.Document {
  _id: string;
  name: string;
  price: string;
  quantity: string;
}
